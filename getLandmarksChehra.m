function [landmarks, bbox] = getLandmarksChehra(test_image, image_name, refShape, RegMat, verbose, folder)

% verbose = false;

% % Load Image
% test_image=im2double(imread([image_path,img_list(i).name]));
% figure1=figure;
% imshow(test_image);hold on;

% % create a thin black line on the top of the image to help detection in
% % bald, white subjects
% test_image(1:6,:,1:3) = zeros(6, numel(test_image(1,:,1)), 3);
% % PROBLEM WAS that face detector was detecting 2 faces in the image
% % SOLVED keeping only the biggest detection rectangle

% % Detect faces
disp(['CHEHRA: Detecting Face in ' ,image_name]);
% faceDetector = vision.CascadeObjectDetector();
% bbox = step(faceDetector, test_image);
bbox = detect_faces(test_image, {'cascade', 'yu', 'zhu'}, folder);
% % If more than one face detected, keep the biggest one
if(size(bbox,2)>1)
    sizes = bbox(3,:).*bbox(4,:);
    bbox = bbox(:,sizes == max(sizes));
end

% % draw face detector's rectangle
if (verbose)
    test_image = insertShape(test_image, 'rectangle', bbox', 'Color', 'red');
    imshow(test_image);
end

test_init_shape = InitShape(bbox,refShape);
test_init_shape = reshape(test_init_shape,49,2);

% % first approximation of landmarks: by relocating the model according
% % to the face detection location
if(verbose)
    plot(test_init_shape(:,1),test_init_shape(:,2),'ro');
end

% % plot numbers
% c = strsplit(num2str(1:numel(test_init_shape(:,1))), ' ');
% dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points
% text(test_init_shape(:,1)+dx, test_init_shape(:,2)+dy, c);

if size(test_image,3) == 3
    test_input_image = im2double(rgb2gray(test_image));
else
    test_input_image = im2double(test_image);
end

disp(['CHEHRA: Fitting ' image_name]);
% % Maximum Number of Iterations 
% % 3 < MaxIter < 7
MaxIter=6;
landmarks = Fitting(test_input_image,test_init_shape,RegMat,MaxIter);

% % plot fitted landmarks
if(verbose)
    plot(landmarks(:,1),landmarks(:,2),'g.');
end

% landmarks = zeros(size(test_image,1), size(test_image,2));
% rows = round(test_points(:,2));
% cols = round(test_points(:,1));
% idx = rows + (cols-1)*size(test_image,1);
% landmarks(idx) = 1;
% imwrite(landmarks, strcat(image_path, strrep(img_list(i).name, '.jpg', ''), '_landmarks.jpg'));
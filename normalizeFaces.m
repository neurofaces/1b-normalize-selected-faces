function normalizeFaces(IODmin)

% % Test Path
% images_path='test-images-cfd/fail-tests/'; % possible-fails
% images_path=fullfile(alg_folder, '/test-images-cfd/');
images_path = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test\';
extension = '.jpg';
verbose = false;
normalized_folder = ['C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized-' ...
    num2str(IODmin) '-IOD\'];

% % Algorithms folder
alg_folder = '../_common_functions/';

% % Display status
disp('Loading models...');

% % Add Chehra folder to the path
chehra_folder = fullfile(alg_folder, 'Chehra_v0.1_MatlabFit');
addpath(genpath(chehra_folder));
% % ---------------------------------------------------------------------->

% % Add CLM-framework folders to the path
CLM_framework_folder = fullfile(alg_folder, 'CLM-framework');
addpath(fullfile(CLM_framework_folder, 'PDM_helpers'));
addpath(genpath(fullfile(CLM_framework_folder, 'fitting')));
addpath(fullfile(CLM_framework_folder, 'models'));
addpath(fullfile(CLM_framework_folder, 'models', 'pdm'));
addpath(genpath(fullfile(CLM_framework_folder, 'face_detection')));
addpath(fullfile(CLM_framework_folder, 'CCNF'));
% % ---------------------------------------------------------------------->

% % List of images in the directory
img_list=dir([images_path,'\*' extension]);
n_images = size(img_list,1);
% % ---------------------------------------------------------------------->

% % Load Chehra Model
fitting_model=fullfile(chehra_folder, 'models/Chehra_f1.0.mat');
load(fitting_model);
% % ---------------------------------------------------------------------->

% % Create structure to store landmarks and bboxes
nland = 98; % 49*2
all_images_landmarks = nan(n_images, nland);
bboxes = nan(n_images, 4);
IOD = nan(n_images, 1);

% % Avoid re-executing algorithm for already processed images
try
    old=load('n');
catch e
    old.ii = 0;
end

% % Get facial landmarks
for ii=1:n_images
    
    % % Avoid re-executing algorithm for already processed images
    if(exist('originalDataToNormalize.mat', 'file'))
        load('originalDataToNormalize');
        break; 
    end
    if(old.ii > ii)
        disp(['Image ', num2str(ii), '/', num2str(n_images), ' skipped - already processed.']);
        try
            load('landtemp');
            continue;
        catch e
        end;
    end
    
    % % Disp status
    disp(['Processing image ', num2str(ii), '/', num2str(n_images), '.']);

    % % Load Image
    test_image = im2double(imread([images_path, img_list(ii).name]));
    if(verbose)
        figure1 = figure;
        imshow(test_image); hold on;
    end
    
    % % Resize image to speed up the process
    tam_resized_h = 512;
    if(size(test_image, 2) > tam_resized_h)
        ratio = tam_resized_h / size(test_image, 2);
        test_image_res = imresize(test_image, ratio);
    else
        ratio = 1;
        test_image_res = test_image;
    end    
    % % Get landmarks and bbox with Chehra algorithm - accurate
    [landmarks_res, bbox_res] = getLandmarksChehra(test_image_res, img_list(ii).name, refShape, RegMat, false, CLM_framework_folder);
    % % Relocate landmarks in original sized image
    landmarks = landmarks_res / ratio;
    bbox = bbox_res / ratio;
    
    % % Store landmarks and bbox
    all_images_landmarks(ii, :) = reshape(landmarks, 1, nland);
    bboxes(ii, :) = bbox;
    
    % % Find centroids of Left and Right eyes
    [~, cxLE, cyLE] = polycenter(landmarks(20:25,1), landmarks(20:25,2));
    [~, cxRE, cyRE] = polycenter(landmarks(26:31,1), landmarks(26:31,2));
    
    % % Eucledian IOD
    IOD(ii) = sqrt((cxLE-cxRE)^2 + (cyLE-cyRE)^2);
    
    % % plot landmarks
    plot(landmarks(20:25,1), landmarks(20:25,2), 'b.');
    plot(landmarks(26:31,1), landmarks(26:31,2), 'g.');
    plot(cxLE, cyLE, '*b', 'MarkerSize', 5);
    plot(cxRE, cyRE, '*g', 'MarkerSize', 5);
    
    % % Close figure;
    if(verbose)
        saveas(figure1, strrep(img_list(ii).name, extension, '_IOD.bmp'));
        close(figure1); 
    end
    
    % % Save temporary results
    save('landtemp', 'all_images_landmarks', 'bboxes', 'IOD');
    save('n', 'ii');
    
end

% % Delete temporary data and save final landmarks
delete('landtemp.mat', 'n.mat');
save('originalDataToNormalize', 'all_images_landmarks', 'bboxes', 'IOD');

% % Minimum IOD (to normalize)
if(~exist('IODmin', 'var'))
    IODmin = 55;
end

% % To save ratios and image names
ratio = nan(n_images, 1);
img_names = cell(n_images, 1);

% % Once landmarks are detected, find IOD and normalize
for ii=1:n_images
    
    % % save img_name to keep the correct order
    img_names{ii} = img_list(ii).name;
    
    % % Ratio to normalize taking minimum as base
    ratio(ii) = IODmin / IOD(ii);
    
    % % Normalize landmarks and bbox
    all_images_landmarks(ii, :) = all_images_landmarks(ii, :) / ratio(ii);
    bboxes(ii, :) = bboxes(ii, :) / ratio(ii);
    
    % % Load Image
    img = im2double(imread([images_path, img_names{ii}]));
    img_res = imresize(img, ratio(ii));
    
    % % Save normalized image
    if(~exist(normalized_folder, 'var')), mkdir(normalized_folder); end
    imwrite(img_res, fullfile(normalized_folder, strrep(img_names{ii}, extension, '_n.bmp')));
    
    % % Inform
    disp(['Image ' num2str(ii) ' correctly normalized and saved.']);
end
save(fullfile(normalized_folder, ['normalizedLandmarks-' num2str(IODmin) 'IOD']), 'all_images_landmarks', 'bboxes', 'IOD', 'ratio', 'img_names');